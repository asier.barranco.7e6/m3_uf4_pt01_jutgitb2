//Llibreries
import java.io.File
import kotlinx.serialization.*
import kotlinx.serialization.json.Json

//Variables de colors
const val rojo = "\u001b[31m"
const val verde = "\u001b[32m"
const val azul = "\u001b[34m"
const val reset = "\u001b[0m"


//Fitxer Json convertit a una mutableList i decodificat
val file = File("./data/problemes.json")
val fileData = file.readText()
val json = Json { ignoreUnknownKeys = true }
@OptIn(ExperimentalSerializationApi::class)
val problemes = json.decodeFromString<List<Problema>>(fileData).toMutableList()


/**
 * Classe que representa un problema amb l'enunciat, els jocs de proves públic i privat,
 * la informació de si ha estat resolt, el nombre d'intents que ha requerit la resolució
 * i la llista dels intents de l'usuari.
 *
 */
@Serializable
class Problema
    (val id : Int,
     val titol: String,
     val enunciat: String,
     val inputPublic: String,
     val outputPublic : String,
     val inputPrivat: String,
     val outputPrivat: String,
     var resolt: Boolean,
     var intents: Int,
     val intentsUsuari: MutableList<String> = mutableListOf()) {

    /**
     * Mostra el joc de proves públic d'un problema semblant per pantalla.
     */
    fun mostrarJocDeProvesPublic() {
        println("Problema $id: $enunciat")
        println("Joc de proves:")
        println("Entrada: $inputPublic")
        println("Sortida esperada: $outputPublic")
    }

    /**
     * Mostra l'enunciat del problema
     */
    fun mostrarExercici() {
        println("Exercici:")
        println("Entrada: $inputPrivat")
    }

    /**
     * Afegeix un intent de l'usuari a la llista d'intents i incrementa el comptador d'intents
     *
     * @param resposta la resposta introduïda per l'usuari
     */
    fun afegirIntent(resposta: String) {
        intents++
        //Afegir l'intent de l'usuari a una llista
        intentsUsuari.add(resposta)
    }

    /**
     * Comprova si la resposta introduïda per l'usuari és correcta.
     *
     * @param resposta la resposta introduïda per l'usuari
     * @return true si la resposta és correcta, false altrament
     */
    fun comprovarResposta(resposta: String): Boolean {
        //Si la resposta que ha introduit l'usuari correspon a l'esperada (la segona del joc de proves privat), retorna true
        return resposta == outputPrivat
    }
}

/**
 * Funció que permet triar un problema de la llista i intentar resoldre'l
 * @author Asier Barranco
 * @version 11/03/2023
 */
@OptIn(ExperimentalSerializationApi::class)
fun escollirProblema() {

    var finish = false
    print("Escoge un problema por su id: "); val opc = readln().toInt()
    if (opc > problemes.size) println("ERROR")
    for (problema in problemes){

        if (opc == problema.id){

            while (!finish) {

                problema.mostrarJocDeProvesPublic()
                problema.mostrarExercici()
                print("Introdueix la teva resposta: ");
                val respostaUsuari: String = readln().toString();
                problema.afegirIntent(respostaUsuari)
                if (problema.comprovarResposta(respostaUsuari)) {
                    problema.resolt = true
                    println("Enhorabona!. L'has resolt en ${problema.intents} intents")
                    finish = true
                } else {
                    println("Vaja! La teva resposta no és correcte. Portes ${problema.intents} intents")
                    do {

                        print("Vols seguir intentant? S/N: ");
                        val seguirIntentant = readln().toString()
                        when (seguirIntentant){

                            "S" -> println("Això continua!")

                            "N" -> {
                                println("A la propera serà!")
                                finish = true
                            }

                            else -> println("ERROR")

                        }

                    } while (seguirIntentant != "S" && seguirIntentant != "N")
                }
            }
        }
        file.writeText(json.encodeToString(problemes))
    }
}

/**
 * Funció que demana les dades d'un problema nou i l'afegeix a la llista i actualitza el fitxer Json
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun afegirProblema() {
    val id = aconseguirId()
    print("Introdueix el títol del problema (un enunciat curt): "); val titol: String = readln()
    print("\nIntrodueix l'enunciat: "); val enunciat: String = readln()
    print("\nIntrodueix l'inputPublic: "); val inputPublic: String = readln()
    print("\nIntrodueix l'outputPublic: "); val outputPublic: String = readln()
    print("\nIntrodueix l'inputPrivat: "); val inputPrivat: String = readln()
    print("\nIntrodueix l'outputPrivat: "); val outputPrivat: String = readln()
    val resolt = false; val intents = 0
    val problemaNou = Problema(id, titol, enunciat, inputPublic, outputPublic, inputPrivat, outputPrivat, resolt, intents)
    problemes.add(problemaNou)
    file.writeText(json.encodeToString(problemes))

}

/**
 * Funció que treu una nota automàticament depenent dels intents i dels problemes que hagi resolt l'usuari
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun reportarFeina(){

    var problemesTotals = 0
    var problemesResolts = 0
    var notaProblema: Double = 0.0
    for (problema in problemes) {
        problemesTotals++
        if (problema.resolt){
            problemesResolts++
            notaProblema += when (problema.intents) {
                1 -> 10.0
                2 -> 9.0
                3 -> 7.5
                4 -> 6.0
                5 -> 5.0
                else -> 2.5
            }
        }
    }
    val notaMitjana = (notaProblema/problemesResolts)
    val notaTotal: Double = (notaMitjana/problemesTotals)
    if (problemesResolts == 0){
        println("No hi ha dades suficients per extreure una nota")
    }
    else println("Nota final: $notaTotal")

}

/**
 * Funció que recorre el fitxer i determina quina és l'ultima ID, per sumar-li 1
 * @author Asier Barranco
 * @version 11/03/2023
 * @return Integer que determina l'ID nova
 */
fun aconseguirId(): Int {

    return problemes[problemes.size-1].id + 1

}

/**
 * Funció que, si els problemes no están resolts, mostra tots el problemes i va preguntant si es volen resoldre, per posteriorment intentar resoldre'ls
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun seguirItinerari() {

    for (problema in problemes) {

        if (!problema.resolt) {

            problema.mostrarJocDeProvesPublic()
            problema.mostrarExercici()
            if (problema.intents > 0) print("Aquest problema ja l'has intentat. Portes ${problema.intents} intents. ")
            print("Vols resoldre el problema? S/N: "); val resoldre: String = readln()
            when (resoldre) {

                "S" -> {

                    var finish = false
                    do {

                        print("Introdueix la teva resposta: ");
                        val respostaUsuari: String = readln()
                        problema.afegirIntent(respostaUsuari)
                        if (problema.comprovarResposta(respostaUsuari)) {
                            problema.resolt = true
                            println("Enhorabona!. L'has resolt en ${problema.intents} intents")
                            finish = true
                        } else {
                            println("Vaja! La teva resposta no és correcte. Portes ${problema.intents} intents")
                            do {

                                print("Vols continuar provant? S/N: ");
                                val seguirIntentant: String = readln()
                                when (seguirIntentant) {

                                    "S" -> println("Continua")

                                    "N" -> {
                                        println("Passem al següent.")
                                        finish = true
                                    }

                                    else -> println("ERROR")

                                }

                            } while (seguirIntentant != "S" && seguirIntentant != "N")
                        }
                    } while (!finish)
                    file.writeText(json.encodeToString(problemes))
                }

                "N" -> {
                    if (problema.id == problemes[problemes.size-1].id){
                        println("Has completat tots els problemes, toro!")
                    } else println("Passem al següent")
                }

                else -> println("ERROR")
            }
        }
    }
}

/**
 * Funció que mostra el progrés de l'usuari
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun mostrarProgres() {

    for (problema in problemes){

        if (problema.resolt){
            println(verde+"Problema ${problema.id}: "+reset+problema.titol+". Intents: "+problema.intents)
        } else if (problema.intents==0) {
            println(azul+"Problema ${problema.id}: "+reset+problema.titol)
        } else println(rojo+"Problema ${problema.id}: "+reset+problema.titol+". Intents: "+problema.intents)

        if (problema.intents != 0) {
            println(problema.intentsUsuari)
        }
    }

}

/**
 * Funció que mostra el menú de tria d'usuari
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun mostrarMenuUsuari() {
    println(
        """
                ||----------------------------------------------------|
                ||                      MENÚ                          |
                ||----------------------------------------------------|
                || 1. Alumne                                          |
                || 2. Professor                                       |
                || 0. Sortir                                          |
                ||----------------------------------------------------|
    """.trimIndent()
    )
}

/**
 * Funció que, una vegada triat l'usuari alumne, mostra i cride les funcions pertinents
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun menuAlumne() {

    do {
        println("""
                ||----------------------------------------------------|
                ||                      MENÚ                          |
                ||----------------------------------------------------|
                || 1. Seguir amb l’itinerari d’aprenentatge           |
                || 2. Llista problemes                                |
                || 3. Consultar històric de problemes resolts         |
                || 4. Ajuda                                           |
                || 5. Sortir                                          |
                ||----------------------------------------------------|
    """.trimIndent())

        print("Tria opció: "); val opc: Int = readLine()!!.toInt()
        when (opc){

            1 -> {
                seguirItinerari()
            }
            2 -> {
                mostrarProblemes()
                escollirProblema()
            }
            3 -> {
                mostrarProgres()
            }
            4 -> {
                val ajuda = File("./data/ajuda.txt")
                println(ajuda.readText())
            }
            5 -> println("")
            else -> println("ERROR. Opció desconeguda")

        }

    } while (opc != 5)

}

/**
 * Funció que, una vegada triat l'usuari professor, mostra i cride les funcions pertinents
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun menuProfessor(){

    do {

        println(
            """
                ||----------------------------------------------------|
                ||                      MENÚ                          |
                ||----------------------------------------------------|
                || 1. Afegir nous problemes                           |
                || 2. Reportar la feina del alumne                    |
                || 0. Sortir                                          |
                ||----------------------------------------------------|
    """.trimIndent()
        )

        print("Tria una opció: "); val opc = readLine()!!.toInt()
        when (opc){
            1 -> {
                afegirProblema()
            }
            2 -> {
                reportarFeina()
            }
            0 -> println("")
            else -> println("ERROR. Opció desconeguda")
        }

    } while (opc != 0)

}

/**
 * Funció que demana una contrasenya
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun contrasenyaProfessor(): Boolean{

    var contrasenyaCorrecta = false
    var intentsContrasenya: Int = 3

    do {

        print("Introdueix la contrasenya: ")
        val contrasenya: String = readLine()!!.toString()
        intentsContrasenya--
        if (contrasenya == "ZinedineZidane10") {
            println("Benvingut. Carregant...")
            contrasenyaCorrecta = true
        } else {
            if (intentsContrasenya > 0){
                println("Error. Contrasenya incorrecta. Queden $intentsContrasenya intents")
            } else println("Error. Contrasenya incorrecta. No queden intents.")
        }

    } while (!contrasenyaCorrecta && intentsContrasenya>0)

    return contrasenyaCorrecta
}

/**
 * Funció que mostra tots el problemes. Depenent de les característiques del problema, utilitza un disseny o un altre.
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun mostrarProblemes() {

    for(problema in problemes){

        if (!problema.resolt && problema.intents>0){
            print(rojo+"Problema ${problema.id}:"+reset+" ")
        } else if (problema.resolt && problema.intents>0) {
            print(verde+"Problema ${problema.id}:"+reset+" ")
        } else if (!problema.resolt && problema.intents == 0){
            print(azul+"Problema ${problema.id}:"+reset+" ")
        }

        if (problema.intents>0) {
            print(problema.titol);print(". Intents: ${problema.intents}\n")
        } else println(problema.titol)

    }
}

/**
 * Funció principal del programa que gestiona una llista de problemes.
 * @author Asier Barranco
 * @version 11/03/2023
 */
fun main() {

    do {
        mostrarMenuUsuari()

        print("Tria usuari: "); val user: Int = readLine()!!.toInt()
        when (user) {

            1 -> {
                menuAlumne()
            }
            2 -> {
                if (contrasenyaProfessor()){
                    menuProfessor()
                }
            }
            0 -> println("Adéu")
            else -> println("ERROR. Usuari no identificat.")

        }

    } while (user != 0)

}